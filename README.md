## Style Guide
https://kowainik.github.io/posts/2019-02-06-style-guide

## Resources
https://animate.style/ -- Animation framework
https://getbootstrap.com/ -- CSS Framework
https://github.com/robert1233/BootstrapColorsExtended -- extra bootstrap colors