module GUI where

import Lib.Prelude 


import qualified Data.Map as Map
import qualified Control.Concurrent.STM as STM
import qualified Graphics.UI.Threepenny as UI

import qualified Domain.Event  as Event
import qualified Domain.Player as Player
import qualified Domain.Spell  as Spell

import qualified Presentation.CDNs      as CDNs
import qualified Presentation.Bar.Init  as Bar
import qualified Presentation.Bar.Operations as BarState
import qualified Presentation.Section.Sidebar   as Sidebar
import qualified Presentation.Section.Topbar as Topbar
import qualified Presentation.Section.Bottom as Bottom
import qualified Presentation.State     as State
import qualified Presentation.State.ZoneMode as ZoneMode
import qualified Presentation.State.Details as Details
import qualified Presentation.State.DamageMode as DamageMode

setup :: State.Data -> UI.Window -> UI.UI ()
setup state@State.Data{..} window = do
    _ <- pure window & UI.set UI.title "AODPS"
    CDNs.add window

    clearButton <- Bottom.mkClearButton
    barHolder <- Bar.mkHolder

    (topbar, battleButton) <- Topbar.create state window barHolder
    sidebar <- Sidebar.create state window barHolder
    -- _ <- Elements.mkFakeBar barHolder

    fork . UI.runUI window $ -- TODO: need to fork?
        UI.on UI.click clearButton $ const $ do
            BarState.destroy window state "backOutRight"
            State.destroyDetails state

    _ <- UI.getBody window & UI.set UI.style [("overflow-x", "hidden")]
    _ <- UI.getBody window UI.#+ map pure [sidebar, clearButton, topbar, barHolder]

    fork $ decrementTimer state window battleButton

    fork . forM_ events $ UI.runUI window . handleEvent state window barHolder battleButton


decrementTimer :: State.Data -> UI.Window -> UI.Element -> IO ()
decrementTimer state@State.Data{..} window battleButton = waitNSeconds 1 $ do
    modifyTVar battleTimerTVar (subtract 1)

    _ <- battleFinished state >>= 
        (`when` UI.runUI window (void (pure battleButton UI.#. "btn btn-primary")))

    decrementTimer state window battleButton

battleFinished :: MonadIO m => State.Data -> m Bool
battleFinished State.Data{..} = do
    timer    <- readTVar battleTimerTVar
    zoneMode <- readTVar zoneModeTVar

    pure $ timer <= 0 && zoneMode == ZoneMode.Battle



handleEvent :: State.Data -> UI.Window -> Bar.Holder -> UI.Element -> Event.Data -> UI.UI ()
handleEvent state@State.Data{..} window barHolder battleButton event = do 
    namesByID' <- readTVar namesByIDTVar
    let lookup id = Map.lookup id $ trace namesByID'

    let startBattle = do
            modifyTVar battleTimerTVar (const 5)
            Details.wipeByZone ZoneMode.Battle detailsMapTVar
            UI.runUI window $ do 
                _ <- pure battleButton UI.#. "btn btn-danger"
                BarState.new window state barHolder
    battleFinished' <- battleFinished state

    case event of
        -- TODO: incorporate second healthupdate into case
        Event.HealthUpdate defenderID attackerID value spellID | value < 0 -> do
            modifyTVar battleTimerTVar (const 5)
            when battleFinished' $ liftIO startBattle
            let update name mode = healthUpdate name (abs value) spellID mode
            case (lookup attackerID, lookup defenderID) of
                (Just attacker, Just defender) -> do
                    update attacker DamageMode.Damage
                    update defender DamageMode.Taken
                (Just attacker, Nothing) -> update attacker DamageMode.Damage
                (Nothing, Just defender) -> update defender DamageMode.Taken
                (Nothing, Nothing)       -> update (show attackerID) DamageMode.Unknown 

        
        Event.HealthUpdate _defenderID attackerID value spellID | value > 0 -> do
            modifyTVar battleTimerTVar (const 5)
            when battleFinished' $ liftIO startBattle
            case lookup attackerID of
                Just name -> healthUpdate name value spellID DamageMode.Healing
                Nothing -> pure ()

        Event.OpJoin playerID -> do 
            Details.wipeByZone ZoneMode.Zone detailsMapTVar

            modifyTVar namesByIDTVar $ \namesByID -> case Map.lookup nameStorageID namesByID of
                Just name -> Map.insert playerID name namesByID
                Nothing -> Map.insert idStorageID (show playerID) namesByID

        Event.OpName name -> modifyTVar namesByIDTVar $ \namesByID -> 
            case Map.lookup idStorageID namesByID >>= readMaybe of
                Just playerID -> Map.insert playerID name $ Map.insert nameStorageID name namesByID
                Nothing -> Map.insert nameStorageID name namesByID

        Event.NewCharacter playerID name -> modifyTVar namesByIDTVar $ Map.insert playerID name

        _ -> pure ()
  where

    healthUpdate :: String -> Spell.Value -> Spell.ID -> DamageMode.Data -> UI.UI ()
    healthUpdate nameOrID value spellID damageMode = do
        liftIO $ addSpell nameOrID (spellID, value) state damageMode
        BarState.update state barHolder

    nameStorageID :: Num a => a
    nameStorageID = - 999

    idStorageID :: Num a => a
    idStorageID = - 998



addSpell :: Player.Name -> (Spell.ID, Spell.Value) -> State.Data-> DamageMode.Data -> IO ()
addSpell name spell State.Data {..} damageMode = do
    detailsMap <- readTVar detailsMapTVar
    atomically . forM_ (Map.toList detailsMap) $ \((_, damageKey), details) ->
        when (damageKey == damageMode) $ 
            STM.modifyTVar details addSpellToDetails
  where
    addSpellToDetails :: Details.Data -> Details.Data
    addSpellToDetails details@Details.Data{..} = 
        details { Details.spellsByName = Map.insertWith (++) name [spell] spellsByName }



