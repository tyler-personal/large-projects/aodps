module Data.ClientParser.ProcessToLines where

import Lib.Prelude
import System.Process (shell, ProcessHandle)
import Control.Arrow ((>>>))

import Lib.Process (readOutputLazily)

import qualified Domain.Line as Line


processToLines :: IO ([Line.Data], ProcessHandle)
processToLines = do
    let albionProcess = shell "/home/tylerwbrown/Apps/./albiondata-client -debug"
    (rawLines, processHandle) <- readOutputLazily albionProcess

--    cleanupAndMark "aopid.txt" processHandle
    let lines = rawLines 
            & filter (not . null) 
            & mapMaybe Line.create
            & filter (Line.variation >>> (/= Just Line.DebugError))


    pure (lines, processHandle)
    
