module Data.ClientParser.LineToEvent where

import Lib.Prelude

import qualified Data.Map as Map

import qualified Domain.Line as Line
import qualified Domain.Event as Event
import Data.Bifunctor (second)

lineToEvent :: Line.Data -> Maybe Event.Data
lineToEvent Line.Data{message} = firstJust $ opNameEvent message `append` do
  tree <- takeAfter "map[" message
    <&> dropLast 1
    <&> splitOn " "
    <&> filter (elem ':')
    <&> filter (notElem '[') 
    <&> mapMaybe (splitOnFirst ":")
    <&> mapMaybe (\(x,y) -> readMaybe x <&> (,y))
    <&> Map.fromList

  (id', name') <- takeAfter ": [" message
    >>= splitOnFirst "]"
    <&> second (takeBefore " ")
    
  name <- name'
  id   <- readMaybe id'
  Event.create tree id name
  
  where
    opNameEvent :: String -> [Maybe Event.Data]
    opNameEvent message = [splitOnFirst "Updating player to " message 
      <&> snd
      >>= Event.create Map.empty (-999)]
