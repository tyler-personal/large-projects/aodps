module Presentation.Section.Topbar where

import Lib.Prelude
import Graphics.UI.Threepenny hiding (map)

import qualified Presentation.State as State
import qualified Presentation.Bar.Init as Bar
import qualified Presentation.State.ZoneMode as ZoneMode
import qualified Presentation.Style as Style
import qualified Presentation.Bar.Operations as BarOperations

create :: State.Data -> Window -> Bar.Holder -> UI (Element, Element)
create state@State.Data{..} window barHolder = do
    battleButton <- mkButton ZoneMode.Battle
    buttons <- ((battleButton, ZoneMode.Battle):) 
      <$> mapM (\mode -> mkButton mode <&> (,mode)) [ZoneMode.Zone, ZoneMode.Total]

    mkListeners buttons

    (,battleButton) <$> div
        #. "btn-group container-fluid"
        #+ map (pure . fst) buttons
        # set style Style.containerStyle
  where
    mkButton :: ZoneMode.Data -> UI Element
    mkButton mode = button #. "btn btn-secondary" # set text (show mode)

    mkListeners :: [(Element, ZoneMode.Data)] -> UI ()
    mkListeners btns =
        forM_ btns $ \(btn, mode) -> 
            on click btn $ const $ do
                modifyTVar zoneModeTVar (const mode)
                BarOperations.new window state barHolder
                mapM_ ((#. "btn btn-secondary") . pure . fst) btns
                element btn #. "btn btn-primary"
