module Presentation.Section.Bottom where

import Graphics.UI.Threepenny

mkClearButton :: UI Element
mkClearButton = button
    #. "btn btn-dark"
    # set text "clear"
    # set style
        [ ("bottom", ".5rem")
        , ("width", "5rem")
        , ("right", "1.5rem")
        , ("position", "absolute")
        ]