module Presentation.Section.Sidebar
    ( create 
    ) where

import Lib.Prelude
import Graphics.UI.Threepenny (Window, UI, Element, on, (#), set, text, style, (#+), div, element, getBody, a)
import Graphics.UI.Threepenny.Events (click)

import qualified Presentation.Bar.Init as Bar
import qualified Presentation.Bar.Operations as BarOperations
import qualified Presentation.State as State
import Graphics.UI.Threepenny.SVG.Attributes (title)
import qualified Presentation.State.DamageMode as DamageMode

create :: State.Data -> Window -> Bar.Holder -> UI Element
create state@State.Data{..} window holder = do
    icons <- mapM (\(str, fullStr, leftMargin, kind) -> mkIcon str fullStr leftMargin <&> (,kind))
        [ ("D", "Damage" , "0", DamageMode.Damage)
        , ("H", "Healing", "0", DamageMode.Healing)
        , ("T", "Taken"  , "3", DamageMode.Taken)
        , ("?", "Unknown", "7", DamageMode.Unknown)
        ]
    iconHolder <- div #+ map (pure . fst) icons # set style
        [ ("height", "100%")
        , ("width", "4rem")
        , ("position", "fixed")
        , ("z-index", "1")
        , ("top", "0")
        , ("left", "0")
        , ("background-color", "#111")
        , ("overflow-x", "hidden")
        , ("padding-top", "1rem")
        ]
    mkListeners icons
    pure iconHolder
  where
    mkIcon :: String -> String -> String -> UI Element
    mkIcon str fullStr leftMargin = a 
        # set text str 
        # set title fullStr
        # set style
            [ ("padding", "6px 8px 6px 16px")
            , ("text-decoration", "none")
            , ("font-size", "2.5rem")
            , ("color", "#818181")
            , ("display", "block")
            , ("cursor", "pointer")
            , ("margin-left", leftMargin ++ "px")
            ]

    mkListeners :: [(Element, DamageMode.Data)] -> UI ()
    mkListeners icons = do
        forM_ icons $ \(icon, damageMode) -> do
            on click icon . const $ do
                forM_ icons $ \(icon', _damageMode) -> 
                    pure icon' # set style [("color", "#818181")]
                modifyTVar damageModeTVar $ const damageMode
                BarOperations.new window state holder
                pure icon # set style [("color", "#ffffff")]
