
module Presentation.Icon where

import Lib.Prelude
import Graphics.UI.Threepenny hiding (title)
import qualified Data.Map as Map
import Graphics.UI.Threepenny.SVG.Attributes (title)

spell :: String -> String -> String -> Int -> TVar (Map.Map String String) -> UI Element
spell spellName spellText altText spellWidth iconMapTVar = do
    iconMap <- readTVar iconMapTVar
    url <- case Map.lookup spellName iconMap of
        Just url -> pure url
        Nothing -> do
            url <- loadFile "image/png" ("static/icons/" ++ takeWhile (/= ' ') spellName ++ ".png")
            modifyTVar iconMapTVar (Map.insert spellName url)
            pure url

    div # set style divStyle #+ 
        [ img # set src url # set style imgStyle # set alt altText # set title spellName
        , p # set text spellText 
        ]
  where
    widthStr = show spellWidth ++ "em"
    divStyle = [("text-align", "center"), ("width", widthStr), ("float", "left")]
    imgStyle = [("display", "block"), ("margin", "0 auto"), ("width", widthStr)]