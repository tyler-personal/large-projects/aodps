{-# LANGUAGe RankNTypes #-}
module Presentation.State where

import Lib.Prelude

import qualified Data.Map as Map

import qualified Domain.Event as Event
import qualified Domain.Player as Player
import qualified Presentation.Bar.Init as Bar
import qualified Presentation.State.ZoneMode as ZoneMode
import qualified Presentation.State.DamageMode as DamageMode
import qualified Presentation.State.Details as Details

data Data = Data
    { events          :: [Event.Data]

    , barsByNameTVar  :: TVar (Map.Map Player.Name Bar.Elements)
    , namesByIDTVar   :: TVar (Map.Map Player.ID Player.Name)
    , iconsByNameTVar :: TVar (Map.Map String String)
    , detailsMapTVar  :: TVar Details.Map

    , zoneModeTVar    :: TVar ZoneMode.Data
    , damageModeTVar  :: TVar DamageMode.Data

    , battleTimerTVar :: TVar Int
    }


create :: [Event.Data] -> IO Data
create events = do
    detailsMap <- f
    forM_ ((,) <$> ZoneMode.all <*> DamageMode.all) (`Details.getOrNew` detailsMap)

    Data <$> pure events <*> f <*> f <*> f <*> pure detailsMap
        <*> newTVar ZoneMode.Total
        <*> newTVar DamageMode.Damage
        <*> newTVar 0
  where
    f :: MonadIO io => io (TVar (Map.Map k a))
    f = newTVar Map.empty

currentDetailsOrNew :: MonadIO m => Data -> m (TVar Details.Data)
currentDetailsOrNew Data{..} = do
    zoneMode <- readTVar zoneModeTVar
    damageMode <- readTVar damageModeTVar

    Details.getOrNew (zoneMode, damageMode) detailsMapTVar

destroyDetails :: MonadIO m => Data -> m ()
destroyDetails Data{..} = do
    zoneMode <- readTVar zoneModeTVar
    damageMode <- readTVar damageModeTVar

    Details.destroy (zoneMode, damageMode) detailsMapTVar