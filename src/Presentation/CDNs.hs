module Presentation.CDNs (add) where

import Lib.Prelude

import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny 
import Graphics.UI.Threepenny.SVG (script)

add :: Window -> UI ()
add window = do
  addBootstrap window
  mapM_ (addCSS window) stylesheets

stylesheets :: [String]
stylesheets = 
  [ "https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  , "https://bootstrap-colors-extended.herokuapp.com/bootstrap-colors.css"
  , "https://bootstrap-colors-extended.herokuapp.com/bootstrap-colors-themes.css"
  ]

_spectreURLs :: [String]
_spectreURLs = 
  [ "https://unpkg.com/spectre.css/dist/spectre.min.css"
  , "https://unpkg.com/spectre.css/dist/spectre-exp.min.css"
  , "https://unpkg.com/spectre.css/dist/spectre-icons.min.css"
  ]

addCSS :: Window -> String -> UI ()
addCSS window url = void $ getHead window #+ [link & UI.set href url & UI.set rel "stylesheet"]

addJS :: Window -> String -> UI ()
addJS window url = void $ getHead window #+ [script & UI.set src url]

addBootstrap :: Window -> UI ()
addBootstrap window = do
  addCSS window "https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css"
--  addCSS window "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
  addJS  window "https://code.jquery.com/jquery-3.6.0.min.js"
  addJS  window "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"

_addSemanticUI :: Window -> UI ()
_addSemanticUI window = do
  addCSS window "https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css"
  addJS  window "https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js"

_addBulma :: Window -> UI ()
_addBulma window = addCSS window "https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css"