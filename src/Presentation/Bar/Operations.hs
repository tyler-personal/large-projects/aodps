module Presentation.Bar.Operations
    ( new
    , update
    , destroy
    ) where

import Lib.Prelude
import qualified Presentation.Bar.Init as Bar

import qualified Graphics.UI.Threepenny as UI
import qualified Presentation.State as State
import qualified Data.Map as Map
import qualified Domain.Presenters.Bar as BarPresenter

new :: UI.Window -> State.Data -> Bar.Holder -> UI.UI ()
new window state barHolder = do
    destroy window state "backOutRight"
    update state barHolder

update :: State.Data -> UI.Element -> UI.UI ()
update state@State.Data{..} holder = do
    details <- State.currentDetailsOrNew state >>= readTVar
    barsByName <- readTVar barsByNameTVar

    forM_ (BarPresenter.barData details) $ \BarPresenter.Data{..} -> do
        case Map.lookup playerName barsByName of
            Just Bar.Elements{..} -> do
                _ <- UI.element barElement 
                    & UI.set UI.style [("order", show order)]
                _ <- UI.element progressElement
                    UI.#. ("progress-bar bg-" ++ show barColor)
                    & UI.set UI.style [("width", percentStr)]
                _ <- UI.element textElement 
                    & UI.set UI.text mainStr
                _ <- do
                    Bar.setDetails detailsElement uniqueSpells iconsByNameTVar
                    UI.element detailsElement 
                        & UI.set UI.style [("order", show order)]
                pure ()

            Nothing -> do
                bar <- Bar.create $ Bar.CreateArguments {..}
                modifyTVar barsByNameTVar (Map.insert playerName bar)

destroy :: UI.Window -> State.Data -> String -> UI.UI ()
destroy window State.Data {..} animationName = do
    bars  <- UI.getElementsByClassName window "progress"
    cards <- UI.getElementsByClassName window "card"

    modifyTVar barsByNameTVar (const Map.empty)

    forM_ (zip bars cards) $ \(bar, card) -> do
        let animation = "animate__animated animate__" ++ animationName
        _ <- UI.element bar UI.#. animation
        _ <- UI.element card UI.#. animation
        pure ()

    waitNSeconds 1 . UI.runUI window . forM_ (zip bars cards) $ \(bar, card) -> do
        UI.delete bar
        UI.delete card
