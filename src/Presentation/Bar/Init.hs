module Presentation.Bar.Init where

import Lib.Prelude hiding (children)
import Graphics.UI.Threepenny hiding (Color, map, RGBA)

import qualified Domain.Spell as Spell
import qualified Presentation.Style as Style
import qualified Presentation.Icon as Icon
import qualified Data.Map as Map

data Elements = Elements
  { barElement       :: !Element
  , progressElement  :: !Element
  , textElement      :: !Element
  , detailsElement   :: !Element
  , detailsShowing   :: !(TVar Bool)
  }

type Holder = Element

type RGBA = (Int, Int, Int, Double)

colorToRGBA :: Spell.Color -> RGBA
colorToRGBA = \case
    Spell.Red -> (231, 76, 60, 1)
    Spell.Brown -> undefined
    Spell.Blue -> (52, 152, 219, 1)

mkHolder :: UI Holder
mkHolder = div 
    #. "container-fluid" 
    # set style (Style.containerStyle ++ Style.flexStyle)

data CreateArguments = CreateArguments
    { holder     :: !Element
    , barColor   :: !Spell.Color
    , order      :: !Int
    , percent    :: !Int
    , mainStr    :: !String
    , uniqueSpells  :: ![(Spell.Name, Spell.Value)]
    , iconsByNameTVar    :: TVar (Map.Map String String)
    }

create :: CreateArguments -> UI Elements
create CreateArguments{..} = do
    textElement <- small 
        #. "justify-content-center d-flex position-absolute" 
        & set text mainStr
        & set style 
            [ ("line-height", "1.5")
            , ("font-size", "1rem")
            , ("width", "90%")
            , ("overflow-x", "hidden") 
            ]

    progressElement <- div #. "progress-bar" # set style 
        [ ("width", show percent ++ "%")
        , ("background-color", "rgba" ++ show (colorToRGBA barColor))
        ]

    detailsElement <- div
        #. "card card-body"
        # set style 
            [ ("display", "none")
            , ("order", show order)
            ]
    setDetails detailsElement uniqueSpells iconsByNameTVar

    barElement <- div 
        #. "progress animate__animated animate__backInUp" 
        #+ [pure progressElement, pure textElement]
        # set style 
            [ ("background-color", "rgba" ++ show (colorToRGBA barColor & _4 .~ 0.5))
            , ("height", "1.5rem")
            , ("order", show order)
            ]

    detailsShowing <- newTVar False

    on click barElement $ const $ do
        showing <- readTVar detailsShowing
        modifyTVar detailsShowing (const (not showing))

        let txt = if showing then "OutLeft" else "InLeft"
        _ <- element detailsElement
            #. ("card card-body animate__animated animate__zoom" ++ txt)
            # set style [("display", "block")]
        
        window <- liftIO $ getWindow holder
        when showing . waitNSeconds 1 . runUI window . void $ 
            element detailsElement # set style [("display", "none")]

    _ <- element holder #+ [element barElement, pure detailsElement]
    pure $ Elements {..}

setDetails :: Element -> [(Spell.Name, Spell.Value)] -> TVar (Map.Map String String) -> UI ()
setDetails detailsElement spells iconMapTVar = do
    _ <- pure detailsElement # set children []
    _ <- pure detailsElement
        #+ map (\(sName, sValue) -> Icon.spell sName (show sValue) sName 4 iconMapTVar) 
            (sortOn (Down . (^. _2)) spells)
    pure ()
