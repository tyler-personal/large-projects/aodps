module Presentation.State.Details where

import Lib.Prelude
import qualified Presentation.State.ZoneMode as ZoneMode
import qualified Presentation.State.DamageMode as DamageMode
import qualified Data.Map as Map
import qualified Domain.Player as Player
import qualified Domain.Spell as Spell

-- TODO: Pull out of presentation
data Data = Data
    { spellsByName :: Map.Map Player.Name [(Spell.ID, Spell.Value)]
    , time :: TVar Int
    }

type Key = (ZoneMode.Data, DamageMode.Data)
type Map = Map.Map Key (TVar Data)

create :: MonadIO m => m (TVar Data)
create = newTVar 0 >>= newTVar . Data Map.empty 

getOrNew :: MonadIO m => Key -> TVar Map -> m (TVar Data)
getOrNew (zoneMode, damageMode) detailsMapTVar = do
    detailsMap <- readTVar detailsMapTVar
    newDetails <- create

    case Map.lookup (zoneMode, damageMode) detailsMap of
        Just details -> pure details
        Nothing -> do
            modifyTVar detailsMapTVar (Map.insert (zoneMode, damageMode) newDetails)
            pure newDetails

destroy :: MonadIO m => Key -> TVar Map -> m ()
destroy key mapTVar = modifyTVar mapTVar (Map.delete key)

wipeByZone :: MonadIO m => ZoneMode.Data -> TVar Map -> m ()
wipeByZone zoneMode mapTVar = do
    newDetails <- create >>= readTVar
    getByZone zoneMode mapTVar >>= mapM_ (`modifyTVar` const newDetails)

getByZone :: MonadIO m => ZoneMode.Data -> TVar Map -> m [TVar Data]
getByZone zoneMode mapTVar = readTVar mapTVar 
    <&> Map.toList
    <&> filter (\((zoneKey,_),_) -> zoneKey == zoneMode)
    <&> map snd
