module Presentation.State.DamageMode where

import Lib.Prelude

data Data = Damage | Healing | Taken | Unknown
    deriving stock (Eq, Show, Ord)

all :: [Data]
all = [Damage, Healing, Taken, Unknown]