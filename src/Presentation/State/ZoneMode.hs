module Presentation.State.ZoneMode where

import Lib.Prelude

data Data = Battle | Zone | Total
    deriving stock (Eq, Show, Ord)

all :: [Data]
all = [Battle, Zone, Total]