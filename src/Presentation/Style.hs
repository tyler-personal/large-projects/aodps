{-# LANGUAGE Safe #-}
module Presentation.Style where

import Lib.Prelude

type Style = (String, String)

containerStyle :: [Style]
containerStyle = 
    [ ("margin-left", "4rem")
    , ("padding-right", "5rem")
    , ("margin-top", ".5rem")
    ]

flexStyle :: [Style]
flexStyle =
    [ ("display", "flex")
    , ("flex-direction", "column")
    ]

