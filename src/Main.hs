module Main where

import Lib.Prelude
import System.Environment (getArgs)
import Graphics.UI.Threepenny.Core (defaultConfig, startGUI, Config(jsPort, jsStatic, jsLog))

-- import Lib.Process (safeCloseProcess)
import Data.ClientParser.ProcessToLines (processToLines)
import Data.ClientParser.LineToEvent  (lineToEvent)
import GUI (setup)

import qualified Presentation.State as State
import qualified Domain.Line as Line

import qualified Data.ByteString.Char8   as BS   (hPutStrLn)
import System.IO (stdout)

-- TODO: Tooltips, animate durations, 
main :: IO ()
main = do
    print 12422226323322533

    (processLines, _processHandle) <- processToLines
--    forM_ processLines (putStrLn . Line.message)
--    forM_ (mapMaybe lineToEvent processLines) print
    startElectron processLines

    --  safeCloseProcess processHandle
    putStrLn "Exited safely."

startElectron :: [Line.Data] -> IO ()
startElectron processLines = do
    [port] <- getArgs
    let events = mapMaybe lineToEvent processLines
    state <- State.create events
    startGUI defaultConfig { jsPort = Just (read port), jsStatic = Just "static", jsLog = BS.hPutStrLn stdout } 
        $ setup state
