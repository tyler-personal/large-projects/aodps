module Domain.Player where

import Lib.Prelude

type ID    = Int
type Name  = String
type TotalValue = Int
