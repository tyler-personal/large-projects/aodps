module Domain.Presenters.Bar where

import Lib.Prelude 
import qualified Data.Map as Map

import qualified Presentation.State.Details as Details -- TODO: Change when moved to domain
import qualified Domain.Player as Player
import qualified Domain.Spell as Spell
import qualified Domain.Spells as Spells

data Data = Data
    { playerName :: Player.Name
    , barColor :: Spell.Color
    , percent :: Int
    , percentStr :: String 
    , mainStr :: String
    , order :: Int
    , uniqueSpells :: [(Spell.Name, Spell.Value)]
    }


barData :: Details.Data -> [Data]
barData Details.Data{..} = let
        startData = Map.toList spellsByName 
            & map (\(name, spells) -> (name, sum (map snd spells), spells))
            & sortOn (Down . (^. _2))
        total = sum . map (^. _2) $ startData
        orderedData = zip [2..] startData
    in for orderedData $ \(order, (playerName, damage, spells)) -> let 
        barColor = fromMaybe Spell.Blue . mostCommon . map snd . mapMaybe findSpell $ spells
        percent = fromMaybe 0 $ (damage * 100) // total
        percentStr = show percent ++ "%"
        mainStr = playerName ++ " " ++ show damage ++ "(" ++ percentStr ++ ")"
        uniqueSpells = Spells.combine $ map Spells.info spells
    in Data {..}
  where 
    findSpell :: (Spell.ID, b) -> Maybe (Spell.Name, Spell.Color)
    findSpell (spellID, _) = Map.lookup spellID Spells.byID
