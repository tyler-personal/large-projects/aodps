{-# LANGUAGE Safe #-}
module Domain.Spell where

import Lib.Prelude

data Color
    = Red
    | Brown
    | Blue
    deriving stock (Eq, Show)

type Name    = String
type ID      = Int
type EventID = Int
type Value   = Int

--type Info = (Name, Color)

type DefenderID = Int
type AttackerID = Int
--type ID = (ID, Value)