module Domain.Spells where

import Lib.Prelude

import qualified Data.Map as Map
import qualified Domain.Spell as Spell


byID :: Map.Map Spell.ID (Spell.Name, Spell.Color)
byID = Map.fromList
    [ (-1  , ("Auto_Attack", Spell.Blue))

    , (1523, ("Poisoned_Arrow", Spell.Blue))
    , (1525, ("Deadly_Shot", Spell.Blue))
    , (1527, ("Multishot", Spell.Blue))
    , (1530, ("Explosive_Arrows", Spell.Blue))
    , (1535, ("Speed_Shot", Spell.Blue))
    , (1537, ("Ray_of_Light", Spell.Blue))
    , (1551, ("Undead_Arrows", Spell.Blue))
    , (1558, ("Raging_Storm", Spell.Blue))
    , (1567, ("Auto_Fire (Main)", Spell.Blue))
    , (1569, ("Auto_Fire (Final)", Spell.Blue))
    , (1571, ("Explosive_Bolt", Spell.Blue))
    , (1576, ("Caltrops", Spell.Blue))
    , (1577, ("Sunder_Shot", Spell.Blue))
    , (1580, ("Noise_Eraser", Spell.Blue))
    , (1586, ("Exploding_Shot (Main)", Spell.Blue))
    , (1587, ("Exploding_Shot (Final)", Spell.Blue))
    , (1595, ("Deathward_Climax", Spell.Blue))

    , (83, ("Burn", Spell.Red))
    , (1705, ("Fire_Bolt", Spell.Red))
    , (1706, ("Burning_Field (Initial)", Spell.Red))
    , (1707, ("Burning_Field (DoT)", Spell.Red))
    , (1708, ("Ignite", Spell.Red))
    , (1711, ("Fire_Wave", Spell.Red))
    , (1714, ("Wall_of_Flames", Spell.Red))
    , (1718, ("Fire_Ball", Spell.Red))
    , (1726, ("Flame_Pillar", Spell.Red))


    , (1960, ("Heroic Strike", Spell.Brown))
    , (1962, ("Heroic Cleave", Spell.Brown))
    , (1965, ("Interrupt", Spell.Brown))
    , (2007, ("Majestic Smash", Spell.Brown))
    , (2009, ("Majestic Smash", Spell.Brown))
    , (2010, ("Majestic Smash", Spell.Brown))
    , (2011, ("Majestic Smash", Spell.Brown))
    ]

info :: (Spell.ID, Spell.Value) -> (Spell.Name, Spell.Color, Spell.Value)
info (id, value) = lookupOr (show id, Spell.Blue) id byID
    & (\(spellName, color) -> (spellName, color, value))

combine :: (Ord k, Num a) => [(k, b, a)] -> [(k,a)]
combine xs = f (map (\(x,_,y) -> (x,y)) xs) []
    where
        f :: (Eq k, Num a) => [(k,a)] -> [(k,a)] -> [(k,a)]
        f [] ys = ys
        f ((k,v):xs) ys = f xs $ case lookup k ys of
            Just _ -> adjust (+ v) k ys
            Nothing -> (k,v):ys

--combineSpells xs = f $ fromList $ map (\(x,y,z) -> (x, (x,y,z))) xs
--        f x | Map.null x = empty
--        f (x@(spellID,_,_):xs) = union (combineSpells xs) $ 
--            insertWith (\(x,y,z) (a,b,c) -> (x,y,z+c)) spellID x xs
--        f (all@(spellID,color,value):xs) ys = union (combineSpells xs) $ case lookup spellID ys of
--            Just _  -> adjust (\(x,y,z) -> (x,y,z+value)) spellID ys
--            Nothing -> insert spellID all ys