module Domain.Event where

import Lib.Prelude
import qualified Data.Map as Map
import qualified Domain.Spell as Spell
import qualified Domain.Player as Player

data Data 
    = OpJoin Player.ID
    | OpName Player.Name
    | HealthUpdate Spell.DefenderID Spell.AttackerID Spell.Value Spell.ID
    | CastSpell Spell.AttackerID Spell.DefenderID Spell.ID
    | CastHit Spell.DefenderID Spell.AttackerID Spell.ID
    | CastHits
    | NewCharacter Player.ID Player.Name
    deriving stock (Eq, Show)

create :: Map.Map Int String -> Int -> String -> Maybe Data
create tree eventID code = case (eventID, code) of
    (-999,  x)             -> Just          $ OpName x
    (2,  "opJoin")         -> OpJoin       <$> num 0
    (6,  "evHealthUpdate") -> HealthUpdate <$> num 0 <*> num 6 <*> num 2 <*> num 7
    (18, "evCastSpell")    -> CastSpell    <$> num 0 <*> num 1 <*> num 3
    (19, "evCastHit")      -> CastHit      <$> num 0 <*> num 1 <*> num 2
    (20, "evCastHits")     -> Just          $ CastHits
    (25, "evNewCharacter") -> NewCharacter <$> num 0 <*> str 1
    _                      -> Nothing
  where
    str :: Int -> Maybe String
    str index = tree Map.!? index

    num :: Int -> Maybe Int
    num index = tree Map.!? index >>= readMaybe

    _errorMessage :: String
    _errorMessage = show tree ++ " - " ++ show eventID ++ " - " ++ code

