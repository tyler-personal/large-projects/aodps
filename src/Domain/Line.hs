module Domain.Line where

import Lib.Prelude

data Variation
    = Info
    | Danger
    | DebugError
    | Debug
    deriving stock (Eq, Show)

data Data = Data
    { variation       :: !(Maybe Variation)
    , timestamp       :: !String
    , message         :: !String
    , raw             :: !String
    } deriving stock (Eq, Show)

create :: String -> Maybe Data
create str = case splitOn "[" str of
    (_:v:_:timestampAndMessage) -> case splitOnFirst "]" (intercalate "[" timestampAndMessage) of
        Just (timestamp, message) -> Just $ Data (variation v message) timestamp (drop 1 message) str
        _ -> Nothing
    _ -> Nothing
  where
    variation v m
        | "INFO"   `within` v = Just Info
        | "DANGER" `within` v = Just Danger
        | "DEBU"   `within` v && "ERROR" `within` m = Just DebugError
        | "DEBU"   `within` v = Just Debug
        | otherwise           = Nothing
