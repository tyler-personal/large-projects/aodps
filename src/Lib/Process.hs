{-# LANGUAGE Trustworthy #-}
module Lib.Process ( safeCloseProcess, cleanupAndMark, readOutputLazily ) where

import Lib.Prelude
import System.Process.ListLike
    ( readCreateProcessLazy, Chunk(Stdout), ProcessMaker(process) )
import Data.List.Split (splitOn)
import System.Process (ProcessHandle, getPid)
import System.Process.Internals (ProcessHandle__(OpenHandle), withProcessHandle)
import System.Posix.Signals (signalProcess, sigKILL)
import System.Posix (CPid(CPid))
import Control.Exception (SomeException, handle)

safeCloseProcess :: ProcessHandle -> IO ()
safeCloseProcess = flip withProcessHandle $ \case
    OpenHandle pid -> signalProcess sigKILL pid -- TODO: Check if this does anything on Windows
    _ -> pure ()

cleanupAndMark :: FilePath -> ProcessHandle -> IO ()
cleanupAndMark fileName processHandle = do
    contents <- readFile fileName

    case readMaybe contents of
        Just pid -> safely . signalProcess sigKILL $ CPid pid
        Nothing -> pure ()

    getPid processHandle >>= \case
        Just (CPid pid) -> writeFile fileName $ show pid
        Nothing  -> error "Cannot write the PID of a dead process"
    

safely :: IO () -> IO ()
safely = handle (print @SomeException)

readOutputLazily :: ProcessMaker m => m -> IO ([String], ProcessHandle)
readOutputLazily maker = do
    processHandle <- process maker <&> (^. _4)
    lines <- readCreateProcessLazy maker mempty <&> formatStdout
    pure (lines, processHandle)

formatStdout :: [Chunk String] -> [String]
formatStdout = (>>= processChunk)
  where
    processChunk :: Chunk String -> [String]
    processChunk (Stdout str) = splitOn "\n" str
    processChunk _ = ["?"]
