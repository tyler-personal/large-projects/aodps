{-# LANGUAGE AllowAmbiguousTypes #-}
module Lib.Prelude.Replicate where

import Control.Monad
import Control.Applicative
import Data.Kind (Type)

replicateM0 :: Monad m => (∀ z. m z) -> m ()
replicateM0 _ = pure ()

replicateM1 :: Monad m => (∀ z. m z) -> m a
replicateM1 f = f

replicateM2 :: Monad m => (∀ z. m z) -> m (a,b)
replicateM2 f = do
    a <- f
    b <- f
    pure (a,b)

replicateM3 :: Monad m => (∀ z. m z) -> m (a,b,c)
replicateM3 f = do
    a <- f
    b <- f
    c <- f
    pure (a,b,c)

-- executeM4 :: ∀ (f :: Type -> Constraint) (m :: Type -> Type) (a :: Type) (b :: Type) (c :: Type) (d :: Type).
executeM4 :: (Applicative m, f a, f b, f c, f d) => (∀ (z :: Type). f z => m z) -> m (a,b,c,d)
executeM4 f = do
    a <- f
    b <- f
    c <- f
    d <- f
    pure (a,b,c,d)

replicateM4_1 :: Applicative m => (∀ y z. m (o (n y z))) -> m (o (n a aa), o (n b bb), o (n c cc), o (n d dd))
replicateM4_1 f = do
    a <- f
    b <- f
    c <- f
    d <- f
    pure (a,b,c,d)

--replicateM4_2
--    :: Applicative m => (∀ z. m ((w :: * -> *) ((∀ k. ww :: k -> *) z)))
--    -> m (w (ww a), w (ww b), w (ww c), w (ww d))
replicateM4_2 f = do
    a <- f
    b <- f
    c <- f
    d <- f
    pure (a,b,c,d)

replicateM4_3 :: Applicative m => (∀ x y z. m (n x y z)) -> m (n a aa aaa, n b bb bbb, n c cc cc, n d dd ddd)
replicateM4_3 f = do
    a <- f
    b <- f
    c <- f
    d <- f
    pure (a,b,c,d)



replicateM4 :: Applicative m => (∀ z. m z) -> m (a,b,c,d)
replicateM4 f = do
    a <- f
    b <- f
    c <- f
    d <- f
    pure (a,b,c,d)

replicateM5 :: Monad m => (∀ z. m z) -> m (a,b,c,d,e)
replicateM5 f = do
    a <- f
    b <- f
    c <- f
    d <- f
    e <- f
    pure (a,b,c,d,e)

replicateM6 :: Monad m => (∀ z. m z) -> m (a,b,c,d,e,f)
replicateM6 f = do
    a <- f
    b <- f
    c <- f
    d <- f
    e <- f
    f' <- f
    pure (a,b,c,d,e,f')