{-# LANGUAGE Trustworthy, AllowAmbiguousTypes, PolyKinds, TypeInType #-}
module Lib.Prelude
    ( module Prelude 
    , module Lib.Prelude
    , module Control.Monad
    , module Control.Monad.IO.Class
    , module Control.Lens
    , module Data.Maybe
    , module Data.List
    , module Data.Ord
    , readMaybe, splitOn
    , TVar, STM
    , (>>>))

 where

import Prelude hiding (div, map, id)
import qualified Prelude as P

import Control.Monad
import Control.Monad.IO.Class
import Control.Lens hiding (set, element, (#)) 
import Control.Concurrent.STM (TVar, readTVarIO, newTVarIO, STM)
import Text.Read (readMaybe)
import Data.Maybe
import Data.List hiding (map, uncons)
import Data.List.Split (splitOn)
import Data.Ord
import Control.Arrow ((>>>))
import qualified Debug.Trace as Trace

import qualified Control.Concurrent.STM as STM
import Control.Concurrent (forkIO, threadDelay)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Kind (Type, Constraint)

trace :: Show a => a -> a
trace a = Trace.trace (show a) a

traceWith :: String -> a -> a
traceWith = Trace.trace

identity :: a -> a
identity = P.id

map :: Functor f => (a -> b) -> f a -> f b
map = fmap

for :: Functor f => f a -> (a -> b) -> f b
for = flip fmap

fork :: MonadIO m => IO () -> m ()
fork = liftIO . void . forkIO 

waitNSeconds :: MonadIO io => Int -> IO () -> io ()
waitNSeconds n f = fork $ do
    let oneSecond = (1000000 * n) in threadDelay oneSecond
    f

mostCommon :: Eq a => [a] -> Maybe a
mostCommon = (^? ix 0) . maximumBy (comparing length) . group

divide :: Integral a => a -> a -> Maybe a
divide _ 0 = Nothing
divide x y = Just $ P.div x y

(//) :: Integral a => a -> a -> Maybe a
(//) = divide

(.:) :: (b -> c) -> (a -> a1 -> b) -> a -> a1 -> c
(.:) = (.) . (.)


firstJust :: [Maybe a] -> Maybe a
firstJust = listToMaybe . catMaybes

adjust :: Eq k => (v -> v) -> k -> [(k, v)] -> [(k,v)]
adjust f k = map (\(kk,x) -> (kk, if kk == k then f x else x))

lookupOr :: Ord k => a -> k -> Map k a -> a
lookupOr x key map = fromMaybe x (Map.lookup key map)

append :: [a] -> a -> [a]
xs `append` y = xs ++ [y]

within :: [Char] -> [Char] -> Bool
within = isInfixOf

takeIf :: Bool -> a -> Maybe a
takeIf True  x = Just x
takeIf False _ = Nothing

splitOnFirst :: Eq a => [a] -> [a] -> Maybe ([a], [a])
splitOnFirst item list = case splitOn item list of
  []     -> Nothing
  [_]    -> Nothing
  (x:xs) -> Just $ (x, intercalate item xs)
--splitOnFirst item list = elemIndex item list <&> (`splitAt` list)

takeBefore :: Eq a => [a] -> [a] -> Maybe [a]
takeBefore = (fst <$>) .: splitOnFirst

takeAfter :: Eq a => [a] -> [a] -> Maybe [a]
takeAfter = (snd <$>) .: splitOnFirst

replace :: Eq b => b -> b -> [b] -> [b]
replace a b = map (\x -> if a == x then b else x)

splitInto :: Eq a => Int -> [a] -> [a] -> Maybe [[a]]
splitInto n x xs
    | n == length ys = Just ys
    | otherwise      = Nothing 
    where ys = splitOn x xs

dropLast :: Int -> [a] -> [a]
dropLast n = reverse . drop n . reverse

modifyTVar :: MonadIO io => TVar a -> (a -> a) -> io ()
modifyTVar = atomically .: STM.modifyTVar

readTVar :: MonadIO io => TVar a -> io a
readTVar = liftIO . readTVarIO

newTVar :: MonadIO io => a -> io (TVar a)
newTVar = liftIO . newTVarIO 

atomically :: MonadIO m => STM a -> m a
atomically = liftIO . STM.atomically 
